# Roadmap

- [x] For gemini to work properly we need only the title. Date, author are optional
- [x] Make a script that properly formats the write.as blog to .md hugo
  - [x] Use the json and/or csv metadata info
    - [x] date published
    - [x] Extract title from filename (regex parsing)
    - [x] We need #tags on body
    - [x] Title
    - [x] Date of creation
    - [x] Autor
  - [ ] Transcripciòn de lenguaje y fecha a validos por hugo
    - [x] Y a la forma con el YAML establecido que tiene ya
  - [x] We just need to append metadata and delete titles if it has
