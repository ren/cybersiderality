#!/bin/sh
cd gemini/converter/
go build \
-v \
-trimpath \
-ldflags="-linkmode=external -X main.version=${gmnhg_version}" \
-o bin/ \
./cmd/gmnhg \
./cmd/md2gmn
