# 11 days

2021-05-26 08:14

I have been thinking in that other person lately, not because of her or anything she told me or that she didn't. But because of my own experiences in these past days, I felt I have done damage, not in the material sense, it shouldn't be damage done by me in this action based material world... yet I still feel I fucked up, that I didn't defend her at the maws of the parasite, but I did took revenge, that was all I could do when I realized... and now I feel she is lost, and separated from me, due to my own weakness and incompetence, I don't seek anything, nor redemption, nor forgiveness, but this heartache is something I couldn't keep so I am letting it out.

This is my heart, wound open brightened by the rays of the sun.

I have fallen into so many of their threads, composed as a spider's web, I did not know because I always thought I was safe, but you can never be safe around someone that seeks control, it will shriek to the bitter end to seize it all, and it will stop at nothing, not the slightest remorse in the harm he's done as he holds control.

If my fears turn true, if they keep on such pitiful path, then the time for avenging will arrive.
