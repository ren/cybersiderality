# There is a burning fire, incandescent, yet finite, it's

2020-11-16 08:14

There is a burning fire, incandescent, yet finite, it's consuming me and my mind, is not rage, is not pain, but love and with each breath that I take my touch, my sense of self fades away, the certainty becomes uncertain, all that because I fail at expressing myself. I feel me slowly converting into a volcano.

And my back, in my back I sense the weight of the world, I was getting used to itt, but now, anew I feel and increasingly stinging sensation... and I am just tired of the imponent mountain I have to climb... tired of everything, anhelating the sweet void, I can barely breathe, my strength leaves me, is this how it feels to die? Complete and utter loneliness, a heavy weight that drowns you in the air as your breathe slowly fades into the sweet coldness of the void?

What am I writting? Why am I writting? Sesations, expresions of the being, because it is something I can't hold in this particular moment, because it is too much for me to even think about it, it is so much and I don't even have the right tools to do what I am required to do, to even process the information that was given to me, and I am still drowning with the air, along, with silence with the intoxicating veil of the night, maybe the only thing truly keeping me alive, the sweet micro death that is my slumber, the beautiful dreams not so rarely imagined, that make me reborn everynight, I really don't like cycles, I really don't like loops, I feel like I am in a cage once again, a crystal cage that moves with me everywere I go, except my dreams, a cage of this world...

And with that burden, I cannot quit. I feel it now, the world even ingraining in my body, what can I do but dream.

I feel the endless permanence of the moment, humans in this world have learned to ignore i, being completely and numb to it, I was too, now no more, one of the many weights I feel is that one, I am aware of the present moment as far as my perception leads me, that's why and *how* I dream outside of the permanece of the moment of my own conscius experience, that is quite literally what dreaming means, another sight, interpretation at totality, trhough mind, through imagination.

I hope this reaches you, and you too can experience the path to freedom, and finally freedom itself. Because that's what we have to do right? Just keep dreaming. No, that's too depressing, I am just too tired sorry. Find a fine muse. This is probably the most sensless or incoherent thing I have ever writen, and still it holds enough meaning to exist, I wonder of who will be able to decrypt it.

All this pessimistic crap is just because I feel sad because other people I care about feel sad, and because I feel the generalized grief of humans around the world. I hear their laments but there is nothing or not much I can do in this moment, with his human body, with this life... and yet I must keep going.
