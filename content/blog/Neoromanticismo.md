---
title: "Neoromanticismo"
date: 2023-02-19T21:16:31-06:00
author: "Lumin"
draft: true
tags: [
    "reflexión",
    "español",
]
---
No es un neoromanticismo, es la culminación de este, entonces es de hecho un postromanticismo pues trasciende la senda del perdedor de este… sin embargo no la victoria le quitaría su magia? - tengo que pensar más sobre esto y sumergirme por completo en el romanticismo para saber que es esta sensación y visión que tengo de culminación del mismo.

Finalmente llegar a la meta, porque es verdaderamente la única opción ahora.

Un todo o nada, se acabó.
