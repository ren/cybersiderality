---
title: "A Dream: El Mounstro Al Final Del Tiempo"
date: 2023-02-19T08:20:37-06:00
author: "Lumin"
tags: [
    "español",
    "sueños",
]
---

## Era un cuadrado en un espacio tridimensional.

Me dijo que iba a olvidar, que me iba a hacer olvidar como lo habría hecho con todos pero esta vez resistí y esto es lo que recuerdo, también debo que decir que lo noté, no es la primera vez que sueño con este objeto, ya lo había hecho antes, pero sí había olvidado.

---

El primer recuerdo del sueño que tengo es que vivía en un mundo como caricatura, con cierta estructura de poder, todo iba bien y todos eramos conocidos de una manera u otra, es decir sabíamos con quien viviamos, como si fuera un pueblo pequeño, aunque en realidad era todo el mundo, en cuanto a la caricatura, pareciamos como si vivieramos en el mundo de los padrinos mágicos, pero de hecho creo que involucraba todos los dibujos de los estudios estadounidenses, no eramos dibujos lindos ni bellos pero era una vida pacífica.

Hasta arriba estaba el tío Stan de Gravity Falls, el controlaba todo o más bien, era la última autoridad y jefe conocido de aquel mundo, sin embargo no era un dictador, sino más bien un vigilante y cada quien hacía lo que quería, como lo dije, era pacifico.

Al menos hasta que vi/sentí dentro del sueño que había pasado algo en esas altas esferas de poder, un desbalance, un evento, un cataclismo que se sentía hasta abajo, hasta los habitantes comunes de ese mundo, de repente un día o al dia siguiente todos comenzaron a pelear entre ellos, todos estaban iracundos, iracibles e irritalbes, como se diría "con la sangre hirviendo", incluso yo me sentía así pero no tenía realmente un oponente, eso es la otra cosa que note, todos se iban contra su nemesis o antagonista e ignoraban casi todo lo demás, en mi caso no estoy seguro porque no tenía con quien pelear pero eso me dió una ventaja, era el as en la manga, uno que tenía claridad sobre la situación y como solucionarla.

Creo que cuando me di cuenta de que algo estaba mal fue dentro de clase, aquí veía el enojo y la furia en el rostro de todos, vi la llama, o más bien parecía un trapo empapado en gasolina, listo para quemarse y estallar y lo hizo, a esto debes imaginar como si fuera un episodio de los padrinos mágicos y yo estuviera simplemente en una de aquellas clases. Y de hecho no se quién inicio aquel fuego, lo siguiente que recuerdo es a mi en cafetería y viendo a todos los profesores y alumnos en una batalla campal, y ahí estaba yo en un momento de resolución, me marché de la escuela y me fui hacía mi casa, de alguna manera supe que todo esto tenía que ver con el dinero y con la injusticia, la ira provenia de que alguién se había llevado todo el dinero del mundo y lo peor, no había un sistema sustituto o razón para aquel acontecimiento y como mencione, sabía donde se había originado el balance. Y no era yo el único As en la manga, sino que yo tenía uno propio.

Era dinero (o eso pensaba), el plan era simple ajustar el desbalance con lo poco que yo poseía, mis ahorros, lo interesante de esta parte es el color y también será importante despues, las monedas deberían ser de cobre como las gringas, pero no lo eran, eran plateadas, lo cual era extraño y no lo recordaba así pues no son las monedas que debería tener, sin embargo no le tomé mucha importancia.

Una vez llegué con el tío Stan todo parecía como sacado de un thriller o una película de terror, todo era oscuro o bueno, más bien apagado y con luces titilantes, y estaba solo él, pero no parecía el mismo, estaba ahí como una figura estatica no animada, sin vida aunque lo estuviera, no hubo dialogo entre nosotros porque no parecia necesario, ambos sabíamos para que estabamos ahí, para arreglar el desastre, le mostre las monedas y en ese momento todo inmediatamente cambió, al ver mi mano donde se supone deberían estar monedas había una masa gris brillante, aún metálica como si estas se hubieran derretido y despues sin que me diera cuenta habían desaparecido, como todo lo demás y todo lo que me rodaba y no había nada más que un vacio enfrente de mi y mi alrededor, todo negro o de un color obscuro salvo un pequeño cuadro en el medio enfrente de mi, literarlmente un cuadrado plateado como las monedas y, debes entender esto, el espacio ahora era tridimensional, pero el cuadrado existía ahí sin ningún volumén.

Através de esta superficie metálica también pude ver el mundo donde vivia, desde fuera, desde dentro, como si pudiera estar en todas partes cuando quisiera estaba en paz de nuevo y el tumulto ya no existía, pero yo tampoco me encontraba ahí, estaba "aquí" ahora. 

Después, escuché una voz, era masculina y mientras estaba en mi sueño no parecia provenir de ningún lugar dento de este, estaba por fuera y la escuchaba dentro, me contó que todo esto había sido obra suya, es decir el desbalance y que ahora yo por traer las cosas de nuevo al balance estaba fuera de aquella realidad, como una especie de premio al final de mi travesía, por mis esfuerzos, esa era mi recompensa, una mirada a la realidad fuera del tiempo.

¿Qué quiso decir con esto?

Bueno que el "lugar" donde nos encontrabamos de hecho no había cosa alguna como el tiempo y existiamos fuera de este.

¿Y cómo?

Bueno esto es sencillo y difícil de explicar a la vez, este cuadrado era la representación entera del tiempo, es decir el cuadrado era el tiempo en si mismo, su maestro y su dios, todo lo relativo a la idea del tiempo y su pasaje pasaba por esta entidad y era la extensión o abstracción completa del mismo como idea, de esta manera poder conversar con él como entidad fuera de si mismo y su existencia aislada implica que el tiempo en si mismo no existiría, pues estaría contenido completamente dentro de él. En terminos simples para hablar con el tiempo este debe dejar de existir para ti.

Al terminar su explicación me dijo que todo volvería a ser como antes, que borraría las memorias de todos como si nada hubiera sucedido incluyendo la mía y tuve la sensación de que este sería el final pues comprendí que donde me encontraba no tenía ni principio ni final, es decir, para llegar al final del tiempo uno bien puede ir al inicio de este, o más bien antes de que iniciara y ahí antes de que existiera el tiempo y una vez se terminara era donde me encontraba yo conversando con este.

Y también pensé que esta intervención no era algo aleatorio, de hecho tenía una razón crítica por haber ocurrido, despues de todo ¿porqué tenía yo un pedazo del tiempo mismo? Pareciera un intercambio y un saber que de hecho siempre estuvo dentro de mi… lo que me intriga aún más es saber de donde surgió eso y realmente cual es la extensión de mi ser, mi pasado y quién demonios soy.

De cualquier manera, me dijo que olvidaría e intento hacerlo enviandome a un mundo paralelo a este donde supuestamente había despertado para que así pudiera olvidar, pero noté que algo estaba mal y desperté después en este mundo… me pregunto si seguiré despertando, de cualquier manera dejo esto aquí como testamento a mi experiencia y a lo que es real incluso dentro de los sueños.

En retrospectiva este ente probablemente me engañó para completarlo o para quitarme algo que me pertenecia, quizá también quiso que recordara, quizá esto fue obra de mi voluntad sola, a decir verdad no lo sé, lo que sé es que se donde y como encontrarlo ahora. Y sin embargo, aunque tenga un completo desinteres y desconexión por la vida no le veo como una forma agresiva o dañina para la vida y la realidad, pareciera más como si tuviera una omnisencia y de hecho eso tendría sentido.