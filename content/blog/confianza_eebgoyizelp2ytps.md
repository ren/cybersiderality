---
author: "Lumin"
title: "Confianza"
date: 2023-02-02T23:51:25-06:00
description: "¿Dónde están los alebrijes?"
tags: [
    "ideas",
    "social",
    "reflexión",
    "español",
]
---
Supongo que es una forma de certeza

Y una promesa sin serlo, una promesa al aire, que no se dice

No es algo que se pida ni que se otorgue

Es algo que existe por sí mismo, que nace de la interacción

Y, por lo tanto, siempre requiere de un otro

No se puede confiar en un algo, porque no hay una reciprocidad

Solo existe una confianza cuando hay una promesa silente detrás

Y solo aparece cuando la promesa se cumple

Y es por eso por lo cual es tan profunda

Por eso nos afecta tanto como individuos

Porque está conectada al corazón, pues no es solo algo emotivo

De hecho, el que sea emotivo proviene del proceso de unión

Porque al romperla se deshace la realidad compartida
――――――――――――――――――――――――――――

And attachment to the world.
This post is about women.

Y la confianza de verdad es de lo más precioso que existe en este mundo, una pura sensación de calidez.