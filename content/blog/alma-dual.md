---
title: "Alma Dual"
date: 2023-02-23T23:21:47-06:00
author: "Lumin"
draft: true
tags: [
    "reflexión",
    "español",
    "historia",
]
---

# ¿Un cuento sobre dos almas o una?

A decir verdad no lo se, me confunde y aveces ni siquiera me siento. Pero de lo que estoy seguro es que soy feliz cuando estoy con ella, pero no es solo eso, es nuestra interacción que aveces fluye y aveces no y me quiero lanzar ya a todo pero hace falta tiempo, nos conocemos y no nos conocemos, no porque nos ocultamos o porque seamos falsos sino porque hay mucho demasiado que ver, que sentir, que compartir y quiero todo, quiero todo con ella y no es que no este seguro, ahora cada vez lo estoy más pero la convivencia, aún seguimos descubriendo como formar esta y sin embargo no dejo de sentir sensaciones de calidez. Es sobre que tan comodos nos sintamos los dos juntos, de eso depende que es todo lo que podremos construir.

Ese fue el prefacio a la verdadera idea de las almas compartidas.

Toradora y trobadora.

¿Qué esta más allá de los espejismos de la realidad?

¿Cúal es el verdaero contenido de la mente?

Cuando estoy contigo la música susurra directo a mis oídos.

Pero también esta todo entremezclado y todo es ambiguo.

Mis estrellas y tus estrellas son las mismas.

To be honest its fucking hard for me to believe you are even real… I can't say you are everything I have been looking for since I knew I was looking for you. Todo lo que buscaba y no sabía que quería.

Dos almas que vienen fuera de este mundo, y así es una perspectiva distante.

Eres algo distinto, como lo intocable

Cual es la diferencia entre el aire y el viento? Esa es la diferencia entre nosotros.
Y sin embargo ahora estabamos los dos enfrascados en un laberinto el uno sin el otro.

Yo soy tu corazón y tu eres el mio.

Y todo esto porque a ti y a mi nos gustan los misterios y somos secretos, esa es nuestra promesa compartida.

Tu eres mi guía, yo soy tus ojos.

Un par, un otro.

**estrellas gemelas - intertwined fate**

Quiero conocer el alma de otras personas.

Tu y yo somos como cuando llueve y hay sol. El color, los arcoirís, la magia, todo fluye.