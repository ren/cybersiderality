---
title: And the angry man in the desert at the end of times
date: 2022-02-09T19:23:43Z
author: "Lumin"
tags: [
    "imported",
    "old blog",
    "write.as", 
]
---


When I spin, what my eyes come in touch with is an angry man, sometimes is a kid, but there is always so much suffering, so much pain, I feel, I hear, their perpetual scream of agony, but it doesn't make any sound, yet you feel it to the bone, in your soul, you don't need physical sensations to transmit emotions.

The man, last time I saw him was looking for something, for the whispers of my own mind and the ones of others, I see a green shirt, I see dark skin, but not from Africa, dark from our own "America", the blood of the natives, mixed and remixed turning his whole face and body into mud.

His eyes white as usual, but with a noticeable angst, he was looking for the whispers, he was looking for what the whispers commanded, but he couldn't find it and this manifested on his gaze, looking around everywhere, asking and asking, yet no answers, the veins on his eyes and his eyelids could be seen, the oppressive tiredness of uncertainty, igniting into frustration and rage

WHERE IS IT WHAT DO YOU WANT ME TO DO

But again, no voices, just feelings and ideas in my head, in my mind as I slowly got into his mind, closer with each spin, each iteration, each loop... we weren't one, nor would I even want to. I was just trying to know what he felt, what he thought, I was trying to understand as I kept spinning my mind off. I was trying to understand why he kept wandering in his mind desolated, why he kept going, or rather and more importantly why are we looping. Came close, but to no conclusions, now however I know who he is, mentally at least.

Was it the demiurge?
It would be too much, this just felt like a hurt child that grew up to a man, but maybe that is precisely what the demiurge is, at any rate it didn't feel as that “divine” serpent, it was just another human, victim of the endless uncertainty. But he was also angry and with a death intend, whole reality feels as if this was his last testament, like if he already died and we were feeling and living his nightmare, I saw we because I felt others, but I didn't see any faces other than his and the back of his body, laying on the ground as if he has been shot in the head.

Fulminated, deleted, but by who?

Maybe he was too a proxy of the real demiurge, because after leaving the spin and going back to "our" reality I started noticing alchemical tampering with my whole environment and inside him I saw a spark of the divine, it wasn't light, but it had the feeling of light outside of our sun, not a physical light, but one you could feel, a trace outside of this world.
