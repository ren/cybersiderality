---
title: Un mundo con una paz absurda
date: 2022-10-23T05:13:43Z
author: "Lumin"
tags: [
    "imported",
    "old blog",
    "write.as", 
]
---


¿Qué sería de mi vida siguiendo la de otros, que sería la realidad sin voluntad?

Mi deber, mi querer ha sido uno de libertad, no solo para mi sino para todo individuo, quiero verles ser y florecer, quiero una realidad personal e interna nunca antes compartida, quiero ver que hay más allá de todas nuestras mascaras.

Pero, siempre desde que esta visión vino a mi me he preguntado como hacer esta realidad ocurrir, más importante aún manteniendo su libertad, pues ¿no es la imposición de mi visión una privación de su capacidad de elección?

Lo he descifrado, por una única y última vez voy a transgredir mis propios principios, para y por la paradoja, haré mi voluntad propia e individual una realidad en otros, pero esta verdad será una de libertad, una reinterpretativa, la realidad del querer, del deseo y del otro, la realidad empática del reconocimiento, la última verdad ejercida por otro, solo una única vez transgredida para hacerla existir... pues de otra manera como siquiera podría atreverme a llamarme humano.
