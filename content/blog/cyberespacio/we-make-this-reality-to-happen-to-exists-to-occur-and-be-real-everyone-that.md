---
title: We make this reality to happen, to exists, to occur and
date: 2021-03-28T03:08:47Z
author: "Lumin"
tags: [
    "imported",
    "old blog",
    "write.as", 
]
---

We make this reality to happen, to exists, to occur and be real, everyone that has been born into this world is an active participant of it, whether desired or not, what I mean by this is that your ego, your body, is not asked to be born but pushed into existence by your parents... and thus the newborn, the act of breeding is not a consensus for all parts for what I currently understand, it's rather a pull to life, an act of ignorance in most cases.

And yet...

I am just an actor of life as you are, in the current moment my pull or strength however doesn't lie on my ego, nor my body, but in my spirit and my soul, that which is completely unseen to the naked material eye... There are forms to see souls and spirits, however, along with other archetypes, but that's not what I wanted to talk about now, but of how I may be a strong actor in the spheres seen by the material, and my pull and will affect the "real" world, but I am not ultimate in any way, I am just me, as you are just you, and as me you exist, and I am sure of it because you are reading this, my point is: take action, for whatever that means to you, be yourself and shine, be a force, be another will in the universe, rise and be alive.
