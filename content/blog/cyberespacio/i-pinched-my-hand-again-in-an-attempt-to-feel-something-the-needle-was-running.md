---
title: I pinched my hand again in an attempt to feel something
date: 2021-09-20T04:53:21Z
author: "Lumin"
tags: [
    "imported",
    "old blog",
    "write.as", "sueños",
]
---

I pinched my hand again in an attempt to feel something, the needle was running with my own hot red blood, I wonder often if I am a machine, people often say machines don't feel, the sad thing is I know exactly why I can't feel, my numbness comes from pain, is like if my heart has burned so hard it is now carbon and even if it were to come out as a diamond it would still be the hardest thing, try to get in, try to make it a scratch, to even break it, nothing, it will only make your own heart bleed.

That's only side one of the disc however, is it disk or disc? What is really the difference? I am not researching that, now deep into the night by tomorrow morning I have to deliver a couple of reports on some lectures, but I can't make myself feel anything, no motivation, no anything, before it used to be fear of my parents, of living in the streets of a dark future, now in the precipice what is the most enticing to me is death, I don't see a reason to keep going, I have a girlfriend but I don't even know how to love, I have a life, I have a family, I have friends, but there is absolutely nothing that makes me feel alive, the only thing I have ever had is my own heart, my burning will, but now that I have extinguished myself, now that even the carbon burns what do I even do? What can I do? I can't touch other people because I am trapped in my sphere, I even thought I could write about this and make something worthwhile of my excistence but my brain doesn't even have the capacity to pull it off, most I can do is this small pages and parragraphs of my depressing thoughts, nothing to actually put in a book, no memories, no structures, I can't make chains of ideas because I hate chains, yet this is one, they come when I less realize it, I wouldn't be surprised if in some years I had absolutely everything and I still couldn't feel anything, the reason I am where I am right now is because I want to feel, to learn about my own mind, learn how to live, but once I am in here what I found is the dullness of existence once again, and this is my own problem because I am not making it better, but really I don't care for making it better because I don't care about anything, not even my own life, again in the abyss, and I can't come out of it because I am the abyss myself.

... A dream lantern? Maybe, but I can't feel it, nor it, nor anything, wish I never existed, and not only now but various times, but I can't die quite yet because I don't even have the will to die, but I don't want to live either, is this how you become undead?

Am I already a zombie that hasn't figured it out? Is this that land.. I wonder, it is draining me, sucking me alive, how can people even live like this, in this?

Hours pass by and I am still in this void doing nothing, why do anything is something I have convinced myself of and I can't really see it wrong. Where is my inspiration, where is my muse, where is my life? Will her even make me feel alive or am I too far gone to be a human?

I want to sleep again, but I won't even be able to do it, my eyes close but my mind persists, why can't I do anything?

I feel something is missing, even in this text, care to tell me what?

Is it life really just lying to yourself until you die? I really can't do that either.

Anyway, coming back to what sparked all this "reflection", I know I have to deliver this report, it is something that I actually need to do to even learn too, I skipped the one from last week, I shouldn't be skipping this, the work is even already half done, I could do it in like an hour or a couple, and is a theme that actually interests me... so then why can't I bring myself to do it? Why am I writing this crap instead of dealing with my responsibilities, why do I have the will to do this which is also writing and even more exhausting than just paraphrasing what other person said, I think I know why in an epiphany just now; I can't do repetitive tasks, I am not an automaton that receives information and turns it into something else, into a regurgitation or interpretation of that information, I can only do that when it is information that concerns me, which I am bound to or related to, an intermediary institution and authorities can't create or link that bond in me, that's why I don't feel it, I know the reason but that doesn't unburden me, in square one I find myself again, but wiser of my own actions.

All this being said, if I can't go to sleep in the next few minutes I will probably make these damned reports.
