---
title: Given how the Gödel incompleteness theorem works with 
date: 2021-09-25T17:42:06Z
author: "Lumin"
tags: [
    "imported",
    "old blog",
    "write.as", 
]
---

Given how the Gödel incompleteness theorem works with math, our universe or rather our current scientific understanding of it only makes sense within a mathematical framework which is in itself unable to prove its own veracity. In other words it means that mathematics and therefore anything build up in top of them are a reasonable and understandable language, but, is in no way an ultimate language or form of communication to understand human experience, reality or the world.

Therefore science is just yet another paradigm of many, although currently very convincing. 
