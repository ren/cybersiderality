---
title: A unsavory persistent feeling of guilt, remorse, it isn
date: 2021-05-26T16:15:46Z
author: "Lumin"
tags: [
    "imported",
    "old blog",
    "write.as", 
]
---

A unsavory persistent feeling of guilt, remorse, it isn't leaving, I wake up everyday and I feel it still there, I feel, I think everything that has gone wrong is due to my own inactions, my own cowardice, but most importantly these derived from my lack of strength, my dimming weakness.

I want to fly with the ones I love, and I have the wings, but I don't know how to do it, it is, it feels sometimes, specially when I am not alone or with people I would trust my heart with... it feels as if I were stripped from my will, from my spirit to fly.
