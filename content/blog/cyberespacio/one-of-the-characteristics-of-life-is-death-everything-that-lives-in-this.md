---
title: One of the characteristics of life is death, everything
date: 2022-01-27T00:24:45Z
author: "Lumin"
tags: [
    "imported",
    "old blog",
    "write.as", 
]
---

One of the characteristics of life is death, everything that lives in this world eventually dies, even things seemingly not as alive like the stars, that's due to the inherent entropy of the world.

However there are things that don't die, those things are not alive, those things are immortals, and they do have a wide range of tricks to allow themselves to exist in this world, however they ultimately fail since most of them require life to even exist.
