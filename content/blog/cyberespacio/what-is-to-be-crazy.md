---
title: What is to be crazy?
date: 2021-09-22T03:43:23Z
author: "Lumin"
tags: [
    "imported",
    "old blog",
    "write.as", 
]
---

What is to be crazy?

As of now, my understandment lays in a misconception or a disagreement of the connotation, value, or importance of the other, being it a subject, an object, or an event.

The mind contextualizes or interprets everything it comes in contact with, however, each mind as it is different and at least has different living experiences referring to the ego depicts its interaction with the other in a different way. Craziness occurs when this interpretation vastly differs from the contextual or collective agreement upon a given object, for example seeing things agreed as red, as blue, more wildly an interpretation of an action of another which _commonly_ would generate no reaction, in the "crazy" subject it could generate aversion.

This means craziness is not a quality of a given being, but just a pointer a stigma given by a group or groups the subject belongs to, ironically in this sense, one could not be possibly crazy on its own, as it is always a social judgment.

So is _society_ quite literally turning you insane?
