---
title: A dream "An Apocalypse in chocolate"
date: 2021-07-22T18:07:11Z
author: "Lumin"
tags: [
    "imported",
    "old blog",
    "write.as", "sueños",
]
---

#dreamjournal

When I first met the kids I was in a bus stop, I was waiting for something not precisely a bus but that's what arrived, it was a road and a stop just in the middle of the desert or maybe it was in a town but all the buildings were abandoned and there was no one else, regardless I hopped in since it was my best shot and shortly after we did went into a deserted area.

Inside the bus there was a kid driving, and there were no adults, it was almost full and it was quite chaotic as you may spect, there was one seat by the back so I sat there, in the next sit there wasn't a kid but his soul felt like the one of a kid he was either Mister Rodgers or Carl Sagan, I don't quite remember what happened next but he became my buddy and I became friend with all kids around me.

After driving for a while in the desert we came around to another town in the middle of the it, but this was a settlement, not deserted there were some other kids, some other scavengers, and there was a community kitchen, meaning they were feeding everyone that came around, one ration per kid, I was by the last of the line because I was one of the last to get in and out of the bus, when it was finally my turn all good shit that was canned was gone and there was a pot of mashed potatoes that was better looking than the alternatives but still no good. These were times of scarcity, and you could only eat what you could get, we were all begars and couldn't choose. However Mr Rodgers and some other kids had some things to put into the mashed potatoes so we did and he had a reasonably tasty meal.

Shortly after we ended dinner one of the guys from the bus came to Mr  Rodgers and me, he was euphoric, we had no idea what he had or what was happening, he wanted to tell us something so he took us to some hidden place in the settlement, took its backpack off, opened it and it glowed as nothing in that world, it was dark chocolate of the highest quality, only used in bakery, we all knew how pricy this would be, for our tounges and in (((money))), the bar was divided in 3 so each of us took a part, it was our deepest secret, but the friend said something else, that backpack is a backpack all kids have but they don't open it, who else knows what's in there, the point is every kid has chocolate bars and so much more and if their were to find out all our supplies would disappear and we would become a target for everyone, so we decided to keep the secret.

Later on the day, now night after we packed up and ended trading with the town we got into the bus and where heading nowhere again in the desolate desert, some guys have figured out the other kid had chocolate, Mr Rodgers and I already ate it, he didn't so they found his piece and were asking for where he got it, almost in a junkie way, then a thunderous impact hit our bus, it was them, the cause of the devastated lands, the rats.

A giant rat has probably sniffed the chocolate and a lot of others smaller of various sizes were also heading to us, once they arrived they were all over the bus on the outside searching for the sacred piece as we remained quietly in the hope of lose their interest, and everyone was on peak levels of stress, the rats ate everything they saw, and they were becoming smarter. After some time they did lost interest although some remained quite close to our bus, driver and leaders deemed safe to begin to move inside the bus and they begin inquiring what could have attracted the rats, the chocolate piece came into the stage, then they will began questioning everyone also giving them an addict look, until they reached Mr Rodgers and me, and with their eye put on me I woke up.
