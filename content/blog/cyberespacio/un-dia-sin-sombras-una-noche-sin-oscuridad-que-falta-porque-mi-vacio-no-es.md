---
title: Un día sin sombras, una noche sin oscuridad ¿Qué fal
date: 2020-12-14T04:32:05Z
author: "Lumin"
tags: [
    "imported",
    "old blog",
    "write.as", 
]
---

Un día sin sombras, una noche sin oscuridad ¿Qué falta? ¿Porqué mi vacío no es oscuro? Quiero descansar, quiero ser feliz ¿Porque no puedo hacerlo? Todo lo que veo, todo lo que siento es el cielo diurno, azul y blanco a los ojos del humano. No me da paz ni tranquilidad, solo esta ahí, en mi inconmensurable y resiliente dolor.

Would you look at the void? What would you do?
