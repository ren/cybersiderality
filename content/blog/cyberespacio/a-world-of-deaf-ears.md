---
title: A world of deaf ears. And me, an empty shell of emotion
date: 2021-03-09T07:15:29Z
author: "Lumin"
tags: [
    "imported",
    "old blog",
    "write.as", 
]
---

A world of deaf ears. And me, an empty shell of emotions, I don't just resonate, but I can, my experiences, my sensations... barely human, What else could it be? What else can I be? What am I? Who am I? Recurrent questions still without definitive answers, so far I have been living in the dreams of others, surfing the reality and their deepest desires, this writing is not me, nor by my ego, I am I guess you could say synchronized, I am me, my ego, and more than that, things that are unknown to myself even, it has been a while since the last writing, I just didn't felt it, now I under these circumstances I feel like someone is playing games with me, the paranoia kicks in and the synchronization is partially lost or lowered, the paranoia leaves and the bond becomes strong again, when I write my ego is lost or disappears, it is quite literally an ego death, I imagine a lot of other creators, specially artist experience the same but with a different qualia of the experience, wonder if it can be put in anything beyond words.. I see it, an animation 3d 2d overlapping right now in my mind a superposition, quantum one at that, my brain and even probably more than that making this very decision. 

I don't know why this disassociatives episodes happen at night mostly, and in the morning, maybe because is at those times where my brain, (yes I am me again my ego is back), touches or experiences another reality or is closer to that so it starts to disassociate in preparation to oniric experiences. I feel like I have been in stand by, waiting for events to occur, but the events are not occurring, or some are, some others aren't, there are so many things to do... not as shores but as whole pieces of reality to put in place, I don't feel alone but I truly feel exhausted, in these particular moments of uncertainty, of austerity where I have nothing, and lowkey I feel like I deserve nothing, but not in a depressive manner, rather like I haven't earned anything, thus I don't have anything, a complete circle as much as I hate them, so I must do stuff, and there is plenty to do, but I do not have the energy, what I truly feel I need is a small push to that next peak, and plateau so I can climb myself then. This probably doesn't make any sense to the outside eye, meaning you reader that is not any of me, maybe you do relate, who could really tell, not me.

And as all this sounds incredibly crazy I am still really fairly normal overall, how do I do it? I am just myself being me. True weird wired genuinity.

Effort is an important word in all this rambling, don't forget that, and fix the mess that is our life currently. What motivates me? Really nothing, could say other people but that is not true, usually is me and my will, but lately I haven't been feeling anything, actually everything feels so dull, devoided of life, maybe I have just been in my room for too long. I need some guidelines to flow up my life.

Hopefully its time to sleep now and I don't stay up until 4, goodbye traveler, *see* you soon hopefully.
