---
title: A twisted sense of justice
date: 2021-05-25T23:16:16Z
author: "Lumin"
tags: [
    "imported",
    "old blog",
    "write.as", 
]
---

A call of fate stumbling down in the dark
The blurred line of codice, revenge and regret.
What is this age of condemn
Damnation to us all in a tounge of spice
Who shall rise and end it all once and for all?

Copies of copies that will never end
A cry for originality mumbled in the void of depravity
What is next when everything that is stands as one
No one left but us, in such empty world of life I want to cry
Am I still me or are you me, who cries? who feels?
I for once stand as another I do not care about my own insanity
Since it is the only thing that keeps me as myself
"Too weird to live, and too rare to die", here I lay in the maws of madness

And this quoute is not of mine...
