---
title: Hypnogaia
date: 2021-07-15T06:21:02Z
author: "Lumin"
tags: [
    "imported",
    "old blog",
    "write.as", 
]
---


Sitting alone in the dark, lost in thought you remember the place beyond, a clear bright place beyond human eyes, not to be seen, but to be felt, you close your eyes, small glimpses of it came back to you, you don't see this place, you don't see it your eyes but with your own mind, a dream to be lived.

Yet gaia, the whole living planet you see her too in your dreams and in her bast lands, all this world is her and about her, she doesn't let you leave, she melts your brain with her hypnotic senses, her melodies, her smells, her touch, her taste, her astonishing views, but most importantly her ideas made reality. 

How to create paradise on Earth? What is even paradise for you? Looking in your heart made of wind, the breeze around all the world inflates a hurricane raises only to be dissolved in the air, everchanging destiny, you in my place, but this speaks too with my own soul, you aren't me, my ideas aren't yours, I am my own, always a problem to the hypnotic grass of thought, I don't know what you are or what am I but I can handle the elements too, a dialogue was opened and we are both now talking, how long will it last? I will not be hypnotized by earthly desire.

And yet I have a splinter of love piercing through all my soul, the time its now and I won't let it become the past as I am for the outside looking here on my reflection where is my live's direction... and I am on the outside looking in to where have I been?
Now say the truth for crawling and I am falling, I have seen the truth... the outside, looking in...
From the outside looking in...

Her song still ringing in my ears making its way to my soul, but I am fully awake now and look her back straight into the eyes, I am the dark caring void.
