---
title: Quizá una de las cosas más especiales y representativ
date: 2023-02-09T20:19:19Z
author: "Lumin"
tags: [
    "imported",
    "old blog",
    "write.as", 
]
---

Quizá una de las cosas más especiales y representativas de México es que somos un país, un colectivo que carga con respeto a la muerte en las orillas de la veneración.

La muerte es algo a lo que no le tenemos miedo, porque no hay tal cosa como soledad, incluso aunque la persona ya no exista en esta realidad colectiva es siempre acompañada, en esta vida y en la que sigue.

Lo que quiero decir es que la simple creencia y acción de día de muertos, de que el alma y/o espíritu permanece de alguna manera, tanto la tradición como la forma mental de la representación de lo que es el día de muerto, es lo que crea la realidad misma del día de muertos y de ese acompañamiento después de la muerte y por eso es que no se siente esa soledad, y es por eso que existe esta confianza en el más allá y por eso no hay temor ni veneración, sino respeto a la misma, de ahí viene, de la comprensión de la muerte y del mito real que nos hemos creado.

Este nexo con la muerte crea un portal a lo sobrenatural, parte del misticismo de México también se origina ahí.

Pero la cristiandad ha roto o debilitado nuestro pasaje al mictlán, porque el mito, el imaginario después de la vida colectivo ha cambiado por el parásito de la cristiandad.

_¿Y dónde están los alebrijes?_
