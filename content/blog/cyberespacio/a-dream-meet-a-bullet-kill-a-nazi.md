---
title: A dream "Meet a bullet, kill a nazi"
date: 2021-09-20T00:43:27Z
author: "Lumin"
tags: [
    "imported",
    "old blog",
    "write.as", "sueños",
]
---

#dreamournal
This happened probably a month ago by now so there may be a lot of details missing or part of the dream that will make even less sense.

I am a culprit of mischief, or was I?
It was so long ago, the only thing I remember now is a metro station and this infamous guy I know, not in person, but in dreams and media, he is an actor, the embodiment of chaos, and he is also me in a sense, we share spirit, so in a sense even if we never met each other, we know the other intrinsically, even to the core, as such when he was in my dream with a Hitlerian outfit, and cosplaying him, I knew exactly through his gaze who was really him, everyone around him saw him as the real one, a new embodiment, but he was just playing, fooling around recklessly as he often does, in the dream he was very known of, he was a danger to the world, he was in the news, and everyone in town knew of this new menace, I, of course, knew too.

It wasn't a menace to the world, not even to me, but we had an unsolved quarrel so when we saw each other at the subway, both unable to stand down the only thing that could result of our encounter was conflict, and so it happened, the fake idol faced me in my darkness, in the dream I was a detective, ironically our suits were basically the same but with colors shifted. He pulled out a gun, I don't remember if I had any with me, but I jumped to grab his, I took it, but as he lost control he seemed to have shot, but his bullet didn't exist, instead, it was his squadron of nazi fanboys defending their master what opened fire upon my body, and all their bullets just felt as one, his, going through my body, a wound in, and a wound out, a hole was now on my belly but I didn't feel any blood, I didn't feel any pain, but I passed out, the last thing I saw was this group of man and then everything getting darker, was I dead?

I woke up, when a friend came to my aid, maybe some hours have passed, I didn't die, I didn't have a wound anymore, but I felt it still, like if I had been shot by a paintball, was the hole in there in my clothes? To tell you the truth I do not remember, shortly after that the dream shifted into others that I have now forgotten, what remains is my thought of death, and not his mercy but my undying rage, and not his will, but the ignorance of everyone that surrounds him as we can't really kill each other, the impostor and me are the same spirit.
