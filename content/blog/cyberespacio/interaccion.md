---
title: Interacción
date: 2021-03-22T23:38:49Z
author: "Lumin"
tags: [
    "imported",
    "old blog",
    "write.as", 
]
---

My mind is so much visual now that it is really hard to put it into words, I see two dots connected by a diagonal line that is ever changing, meaning the original dots are moving and also connecting to others, that is interaction for me, nodes.
It looks and seems simple and it mostly is, but there are certain scenarios where the interaction comes as deep as the mind so the sharing of information becomes a fusion of the dots and the lines connected, it is no longer a node but a temporal structure of consciousness. This can happen with psychoactive drugs, this event is called transference, and it is as well something that is not those precise events but rather how reality itself to the human mind is structured, a a mental construct of different parts, different minds, different people, egos, souls and spirits. 
