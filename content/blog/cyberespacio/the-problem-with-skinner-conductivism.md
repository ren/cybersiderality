---
title: The problem with Skinner conductivism
date: 2021-11-23T00:48:15Z
author: "Lumin"
tags: [
    "imported",
    "old blog",
    "write.as", 
]
---


I think the main issue with it is the own setting of the experiments, the _ideal_ isolation in which they take place is something nonexistent in the _real world_, quite the contrary it is quite an abnormality, an anomaly and as such the behavior is not natural but dictated and conducted of course by what little stimuli the subjects of the study are, in other words the setting is an anomalous one and as such the behavior of the subjects is faulty or rather abnormal in relation to how they would normally behave, which creates itself one kind of self-fulfilling paradox or feedback loop in which its hypothesis is self validated but only because the data obtained is on a context or a system which allows these proposals to be valid. A specie of meta-language of which mathematics and science itself also suffers.
