---
title: Some dreams
date: 2021-06-30T04:11:03Z
author: "Lumin"
tags: [
    "imported",
    "old blog",
    "write.as", "sueños",
]
---

I dreamed I was a vampire
#dreamjournal

It is cold, my eskimo suit is on and I am still trembling, I do not dislike this, I love the rain, I love the freshness and I don't want it to stop, it has been so long since an actual rainy summer.

On this night, almost early in the morning... my dreams are the one of a vampire, I find myself on an alley it is a crepuscular time, not quite night, but there isn't any imposing sun casting shadows, yet I encounter myself with a very dark figure, I do not know who this is, but we have a exchange of words, of ideas, and he gives me a look of worthiness, then under his cape he reveals himself, it is like a vampire and he gives me one dark thing dropping off his arm/wing, I take it.

Later on the dream it is as if I were playing a game, defeating enemies and surpassing obstacles I reach the very last level, I have many powers like speed and strength, vision and enhanced senses. The last level is just a circle and on the other extreme there is a wide beast, a completely white vampire, and along side there is a young white man, just like Alucard from Castelvania.

The beast as soon as I was in the middle of the circle charged unto me, it could not defeat me, not even touched me, there was like a field or a force repealing his attacks, then I sensed the fear in his eyes, in his own body and aura, it immediately knew the only thing that could prevent him form attacking was his former master, it was very confused and afraid, it retreated. The Alucard dude noticed, and came quickly close to me in an instant, he was intrigued by me was just walking in circles around, until he stopped once he figured out what I was. I was mantling his father, or the other but without being it, I was just wearing a suit of power (the dark goo) enhanced and compatible with my own will, he acknowledged that I wasn't the previous bearer thus no sin was on my spirit nor in my soul, I woke up now with memories and somehow related now to Alucard.
