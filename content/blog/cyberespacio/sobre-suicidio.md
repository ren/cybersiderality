---
title: Sobre suicidio...
date: 2020-10-17T17:52:36Z
author: "Lumin"
tags: [
    "imported",
    "old blog",
    "write.as", 
]
---


No sabía porque mi comportamiento era como el del viento... nómada, un viajero, y sin embargo estancado en este lugar no me gusta dejar raíces, porque nunca permaneceré en un sitio por mucho tiempo, porque mi hogar es el camino.

No tengo miedo ni problema con que la vida no tenga ningún sentido, de hecho estoy completamente de acuerdo con eso, desde una perspectiva de diseño me parece una decisión excelente. La vida, en esta realidad, en este mundo, un sandbox en el que puedes hacer lo que quieras con los materiales dados por tu cuerpo y por el propio mundo, es un poco incredible, pero vaya que ha sido tan corrompido y trastornado, lo que era un juego de niños, esta vida humana, o vida en general en este planeta, en este universo, un juego de descubrimiento interno y externo... ha cambiado al menos para la perspectiva del humano a... no sé ¿Porqué siquiera trabajamos? ¿Porqué siquiera vivimos? Es decir ya se que no hay propósito y eso esta bien, pero aún así atribuimos valor, al dinero y vivimos y morimos por él ¿Porqué? Lo peor de todo es que de una valencia neutra de existencia o vida, es decir siempre estamos persiguiendo algo, vivir entonces no esta dado por defecto y debe ser algo alcanzable... y esta bien si esto es bajo las propias leyes naturales de flujo energético (alimentación, desechos etc.), pero no es así, ya no más para el humano, no somo ya regidos por estas leyes, no hay caza, no hay recolección no hay desarrollo por vivir, nos hemos creado una casa de instituciones que suplantan nuestras necesidades naturales y en cambio nos dan ... ¿? ¿seguridad? o eso es lo que venden, la verdad es que no nos otorgan nada, sino nos lo venden a cambio de nuestra propia vida, de nuestra propia experiencia humana, usando la moneda y ese, es un mundo, una idea en la que no estoy dispuesto a vivir, pues lo único que existe es la miseria y no estoy seguro siquiera que el deseo pueda anteponerse a ella.

A lo que me refiero con el deseo es a lo que nos motiva a vivir, lo que provoca que queramos existir, de nuevo esta bien si lo que existe es un vacío, pues es valencia neutra, pero si eres forzado a vivir, a existir y tu no estas haciendo una elección al hacerlo, te encontrarías de nuevo en el espectro negativo de la existencia. Como ahora, ¿porque vivimos ademas de los frutos que nos han dado estas superestructuras humanas, estas instituciones?
