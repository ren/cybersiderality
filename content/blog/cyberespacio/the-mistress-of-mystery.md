---
title: The mistress of mystery
date: 2021-05-30T21:41:25Z
author: "Lumin"
tags: [
    "imported",
    "old blog",
    "write.as", 
]
---


She is charming and daring, but she is a secret.

Mystery is only interesting as long as it is kept unknown, once it is revealed all her charm is lost.

Let's talk about big foot. It being real or a complete hoax is irrelevant, its charm comes from its own uncertainty, once its veracity is settled the myth the legend dies and it is replaced by normality and commonality, it no longer revers awe and amusement, it is no longer a treat for the mind, a play and exercise of imagination, it transforms into just another thing we are "sure" of, a "truth" a "certainty".

Oh lady of secrets, oh Ino.
