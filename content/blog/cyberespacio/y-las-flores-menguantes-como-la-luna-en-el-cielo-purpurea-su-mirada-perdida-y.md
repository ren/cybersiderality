---
title: Y las flores menguantes como la luna en el cielo, purpu
date: 2020-10-23T18:06:49Z
author: "Lumin"
tags: [
    "imported",
    "old blog",
    "write.as", 
]
---

Y las flores menguantes como la luna en el cielo, purpurea su mirada perdida y difusa en la oscuridad de la noche. Un día vacío, una vida vacía, pero sin ser nada, siempre elusivo de la ausencia de existencia, excepto cuando la mirada se pierde y los campanales se cierran, más allá de la oscuridad, más allá del vacío se encuentra la nada indescriptible, inimaginable y atemorizante, inexistente ser.

Abres los ojos, despiertas de nuevo, otra mañana, otro día sin sueños.
