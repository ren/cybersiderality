---
title: Las noches raramente están en silencio
date: 2022-10-23T18:23:03Z
author: "Lumin"
tags: [
    "imported",
    "old blog",
    "write.as", 
]
---

Aullidos de dolor y sombras
Pasión y euforía más allá del velo del crepúsculo
En la monotonía de los días uno descansa
Pero es no tambíen una monotonía el propio despertar
Que hay más haya del sombrio día
Y de la tan tardía noche
¿Es qué no hay nada más que luz y oscuridad?
