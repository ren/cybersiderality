---
title: Visions of the Esher
date: 2021-04-06T00:47:40Z
author: "Lumin"
tags: [
    "imported",
    "old blog",
    "write.as", 
]
---


I am hardlocked. I see it clearly, I was defeated by my own curiosity, by my own fear and my own uncertainty, but in this defeat, although here in the musk I lie in a deep depression,  I know I haven't lost, I still breathe, I am still alive and most importantly I am aware exactly of how to solve the nightmare I witnessed, and I also know what caused it at least to resurge in my last direct experience with the unconscious world, it was my own instability along with the difference of presence in each mind I was diving along with, it was something I was not planning and for which I was not prepared, yet I did felt that slight rush of people expecting something from me, that sight, that sensation I can't quite put a word on tingling all my spine.
It was also a testament of anger I rejected my anima and that was her revenge, I see it know thinking back in the events, and the dreams I have been having since them, which have been a way of reconciliation, I still don't know who is she, yet I feel from almost everyone people wanting me to make couple with people that aren't for me, they do not know that, they rather see me complacent with a any partner than with my missing partner, I know they mean well but it really just troubles me more.

I am now just been tossed around like a bitch, stripped of all power, of all will, but is not really that, I am just experiencing, I am letting things happen because I haven't felt them before and only then once experienced I can tell if it is something I like feeling or not, for me there is really no other way to decide than knowing, this, is my gnosis.

One thing I long for however is true direct interaction in these matters and not the usual unaknowledging of the self, everyone always playing dumb, why? Even if you don't know what to say, that's exactly what you can and should say, honesty.

The hardlock now comes from my apparent defeat, from my state of mind, I am it the bottom and no one expects anything from me because I failed when I was supposed to shine, but if not me, my spirit always raises and rekindles, right now I am without my wings or rather without my eyes, and to fly and see again I must get out of my own darkness, as I said before I can still see, but those are not my proper eyes, would be like cheating, in my mind I can only properly get then back by also rising from the ashes in this world, meaning the kickstarter, the ember to rekindle hope comes from the actions of my body and ego in this world.

I can give hope from and to myself right now with my fake eyes and invigorate me and revitalize me for what is to come or I could get up from my abyss in a month of constant focus and improvement. If I do the first and I fail in this world, that's it, I will be stripped from my own spirit, or it will rather leave and not be associated with me again, its like losing the core of your own soul... if I do the second one and succed I will be stronger and most certain than ever, if I fail my spirit may leave or not, but our bond will be damaged, but not completely striped nor broken, it may even shift and change but it won't be as drastic as the other. If I do rekindle my spark now and succed later it will elevate me but it won't be nearly as strong as it coming from my actions here. So with that in mind the best option seems for me to be focused here, at any rate what happened was a mistake on my end, if I think of all the other probabilities that could have occurred, still the optimal course of action is the focus on my current ego and body, I just, as I remember just now... fell into the ground, and are ready to get up again, and also as in that time, it was intentional now, mistakes were made and now is time to solve them. 
