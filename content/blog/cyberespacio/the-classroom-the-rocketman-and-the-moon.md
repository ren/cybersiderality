---
title: The classroom, the rocketman and the moon.
date: 2021-05-05T18:21:25Z
author: "Lumin"
tags: [
    "imported",
    "old blog",
    "write.as", "sueños",
]
---

A dream

I feel like writing this in spanish so feel free to omit it or use a translator.

Al despertar me he quedado con la memoria de los sueños pero he decidido no ir directamente a escribirlo, no estoy seguro porque, no lo sentía como algo apremiante y al mismo tiempo todos los contenidos ya los había descifrado así que no había razón más alguna que compartirlos, eran sueños fantásticos e increíbles y he decidido quedarme con la memoria ya olvidada de ellos y apreciar ese momento que ya no existe con todas sus sensaciones y contenidos a forzarme a cimentarlo y pegarlo a un pedazo de texto digital.. aún recuerdo un poco, son como pequeñas trazas de visiones... no se aún si debería escribirlo.

Part one, the classroom

Me encuentro en un salón, hay chica, no conozco su rostro, no lo recuerdo, no solo somos nosotros dos pero ella parece ser una amiga y la única persona a la que le tomo importancia, por dentro el salón se parece un poco a los de mi primaria, tabiques cafés como barnizados color canela dulce, un poco de blanco, ventanas en la parte de arriba lateral, por fuera los mismos colores estamos en un pequeño edifico de dos pisos en el que hay ocho salones en total, incluyendo un par de oficinas, una por cada piso, yo estoy en el salón justo antes de la oficina el penúltimo y parece que estamos tomando un examen o.. es la revisión de un examen? De cualquier manera la chica con la que sueño lo pasa y es curioso porque creo que es la novia de un amigo y saber que ella lo pasa me da certeza de que yo también lo haré, esto quizás va relacionado un poco al estrés que tengo por los próximos días.

Part two the moon and the spaceship

Después del sueño anterior transiciono a otra mascará ahora yo soy Gordon Freeman (si el tipo de Half Life, un poco irónico también) y también esta Alyx, pero no es la del juego, es... se siente como una persona real que conozco, no recuerdo mucho de esto, siento que la debía rescatar, no espera, recuerdo ahora, no era Gordon Freeman, yo era Spike y ella era Faye, y estábamos en lados opuestos de la nave o una estación espacial infestada con zombies mutantes radioactivos alienigenas o algo así, recuerdo que brillaban de verde fosforescente en algunas partes, como si tuvieran baras radioactivas saliendo de ellos o como si algunas partes de su cuerpo brillaran. No recuerdo mucho más que eso y que había un gran problema sobre el riesgo que ambos corríamos pero no nos importaba al final nos encontramos y nos abrazamos y recordé esa escena del anime donde Spike se marcha y Faye intenta detenerlo.

Y creo que había una tercera parte pero ya ni estoy seguro, quizás he mezclado las 2 últimas ahora, si logro recordarlo lo escribiré.
