---
title: Un día azul, un día soleado
date: 2022-02-15T22:18:02Z
author: "Lumin"
tags: [
    "imported",
    "old blog",
    "write.as", 
]
---

Bajo el colibrí y sus alas se esconde un brillo
Un enigma, un misterio falso
¿Qué es? ¿Qué murmuros grita?
¿Cuál es la grilla? ¿Cuál es la guía?
Y dias eternos sin espera al batir de sus alas
¿Qué es lo que dice? ¿Qué es lo que sopla?
¿Cómo es el sonar del viento azul del colibrí?
