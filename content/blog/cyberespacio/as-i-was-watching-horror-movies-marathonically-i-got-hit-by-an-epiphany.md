---
title: As I was watching horror movies marathonically I got hi
date: 2021-11-02T02:04:05Z
author: "Lumin"
tags: [
    "imported",
    "old blog",
    "write.as", 
]
---

As I was watching horror movies marathonically I got hit by an epiphany. Authority, it is a core concept in while designing the narrative of a horror film or story, at some point of development a decision must be made if you want to involve a certain type of figure with authority, being it the police, parents and even the army, which they often disregard the appearances of the supernatural (that on its own deserves its own reflection, but for today we will focus in the role of authority in horror films).

I believe it is something mythic, and intrinsically a tradition of the west related to Christianity, in this sense, authority is the savior in the context of a horror movie since it is the idea of authority being something divine is the ultimate power in metaphysical or supernatural intervention or interactions in common life, which is too where this kind of stories occur, they are often a nexus between what is commonly understand as day to day life and the _unreal_. In this way the aspect of divinity toned down to the idea of authority in the junction of these nexuses and this is why authority of any kind in horror is such a impactful choice when _constructing_ a narrative, because in a metaphysical setting the divine is "what sets things right". It is as well, the imperative of order. This all being too a human construction, our order then, is the day to day life, which in these scenarios is always the desired outcome, but I wonder, why must we thrive to order? Seems like a tautology, order, and ordered life, which desires only more order when in adversity. Was it the divine too? That which keeps us safe, which keeps us locked and trapped.
