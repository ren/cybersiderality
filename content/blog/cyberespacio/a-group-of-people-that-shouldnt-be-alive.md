---
title: A group of people that shouldn't be alive
date: 2021-11-20T00:57:56Z
author: "Lumin"
tags: [
    "imported",
    "old blog",
    "write.as", 
]
---


I have this idea, kinda forgotten now, but it came out of a feeling, a feeling of death, or rather a place where everyone is dead or should be, but they are just carrying on with their lives, their monotonous tasks, why? Why does this place exist? What are souls doing here? Or maybe... there are no souls existing in this desolate place, at any rate I forgot and lost the original idea, and it feels like crap, like a dissonant song in the back of my head, not a noise, not strident, but the quietness of incompleteness.

They are not zombies, but they seem like some.

Then I remembered another group of people, those that did not die, but should have, and yet exist, some outsiders, some lucky, some protected by the thread of destiny. It does not matter, I see them in the dark, just roaming around in the caressing void of my palm, sometimes I whisper to their ears, sometimes I just contemplate their little orb of being, my pets? My friends? My soul. They swim in it and they fly. They are alive. they shine like the stars.

...

If I ever remember, I will share it as soon as I do.
