---
title: El extranjero
date: 2021-11-20T00:42:13Z
author: "Lumin"
tags: [
    "imported",
    "old blog",
    "write.as", 
]
---


What is to be a stranger? Or... what is to always be the stranger.

A passing leaf, the wind itself going everywhere and yet nowhere, surrounding us, completely unacknowledged, completely noticed, who?

Always  on the top of the mountain, the hermit ponders upon the visage of the world, what does it see when it comes down, what does it see when it goes up? For your eyes you do not have, do you reckon loneliness? Do you reckon sorrow? Do you reckon sadness? Do you reckon the void itself?

These are your eyes when you belong nowhere you, see every little house, every country, every palace, every tent, their mental caves, their houses, it is cozy, it is warm, but when you see them, they look you back, and what was nothing now is noticeable, eyes opened in the void, but they aren't looking outwards, there aren't yours, they are looking inwards, you are being watched. What will the weird one do?

Things is, most people have been strangers, but they don't remain that way, because they never were in the first place, they were just unrecognizable, they were the new that then got into the club, their strangeness was only momentarily, a gasp, a glance into being the abyss, but no one can hold it, and the ones that do never come back, for they are of nowhere now.

They have seen you, which is nothing on its own but, you are now the cause of their bad augurs, in fact, you are the bad augur, so you leave again, you take flight with your pompous wings, in the dark night as strange as you arrived equally rare you left, and then settlements become taverns and hostels, their inhabitants their community, all there all real, with all sorts of characters, but everywhere you go, the constant that remains is the newcomer, the outsider.

In the end, you just keep walking, your home is the road even if you don't want it, maybe we are all strangers until we settle down, but in running forever we don't know how to stop anymore, unable to set root, to create bonds one with the lonesome road. 

He, who everywhere he goes is a stranger to the known, he who everyone looks, he who everytime is signaled out, he who never knows the tongue, he who never knows the songs, he who never feels in home, he who cannot create any bonds, now, the only way is forward.

I may do a part two or edit this one way more descriptive, for now, goodbye, and safe travels.
