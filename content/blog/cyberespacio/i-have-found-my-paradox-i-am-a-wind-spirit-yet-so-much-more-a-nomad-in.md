---
title: I have found my paradox, I am a wind spirit, yet so muc
date: 2020-10-17T18:48:36Z
author: "Lumin"
tags: [
    "imported",
    "old blog",
    "write.as", 
]
---

I have found my paradox, I am a wind spirit, yet so much more, a nomad in clearer terms, that prevents me form attaching to people, yet, what I desire most is companionship, real connections, camaraderie, crewmates to sail beyond the stars into infinity.

But I am here on my isolation capsule unable and unwilling to get out, now however, I am aware and I can evolve, and I can keep moving forward and solve my paradox.
