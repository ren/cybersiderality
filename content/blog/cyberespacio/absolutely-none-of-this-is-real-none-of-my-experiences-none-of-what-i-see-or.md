---
title: Absolutely none of this is real, none of my experiences
date: 2020-12-09T21:40:23Z
author: "Lumin"
tags: [
    "imported",
    "old blog",
    "write.as", 
]
---

Absolutely none of this is real, none of my experiences none of what I see or conceive as real is, I know this, I know this is the truth, but I keep up the illusion because it is easier to get by this way, to live, to exists, to comprehend, reality and everything, and this is not just me, I hope it is, but I don't know if it is, my fear is that it is like this for every human in this world, the mediocrity, the easiness of this world has slowly strangling our mind, our imagination, our will, our dreams, and yet we keep the illusion going, why, why can't I shut it off? Is it a self sustainable collective delusion or is it just mine? How do I snap out if it? I know the answer or at least where to find it, but I am not ready yet, and being honest with you I am still quite afraid... and no, it is not death, that's too reckless, even for me, death is a sentence it is the grand beyond, I have some tools, some substances to play death while not actually being, a sneak peak at a existence without a material body in this world, or I don't even know if that is the truth yet, that is still just an idea, as I said, that is just where I was lead to, I have yet to experience it.
