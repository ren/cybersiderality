---
title: La gran tarea, lo complejo, de lo que me doy cuenta jus
date: 2023-02-03T05:08:01Z
author: "Lumin"
tags: [
    "imported",
    "old blog",
    "write.as", 
]
---

La gran tarea, lo complejo, de lo que me doy cuenta justo ahora, es que la única forma de vencer, de generar un cambio y una revolución verdadera no es solo eliminando el dinero de la mente colectiva. Si no sustituyéndolo por otro lenguaje, uno verdaderamente humano que nos permita comunicarnos más que con números.
