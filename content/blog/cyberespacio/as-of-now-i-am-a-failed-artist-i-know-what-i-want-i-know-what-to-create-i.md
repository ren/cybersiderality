---
title: As of now, I am a failed artist, I know what I want, I 
date: 2021-03-29T07:14:54Z
author: "Lumin"
tags: [
    "imported",
    "old blog",
    "write.as", 
]
---

As of now, I am a failed artist, I know what I want, I know what to create, I know the dreams I want to exist and live in, but I don't know how to do it, I am incapable of making them, I am engulfed in so much hatred, pain sometimes, suffering, so I cannot create beautiful things with and out of joy, my creations are damaged as myself.

I am however a sculptor, I see and I deeply feel what this world and what other works of creation need to become a shining star, I can from a boulder create an ever-shining diamond, I am in this way a creator, a refiner of dreams, I am a fixer of (uni)-verses.

My hope is that one day I can let go of the hatred and pain that has impulsed me for ages, and I can finally rest, and create beautiful existences.

This is talking about my ego, other elements of myself can and are creators not attached to the particular suffering of this life, my "ghosts" do create, but it is not me who creates, it is not me who controls or who directs such creations, as my ego is a sleeper observant of the dances of my spirits.
