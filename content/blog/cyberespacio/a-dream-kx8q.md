---
title: A dream
date: 2021-05-30T15:22:05Z
author: "Lumin"
tags: [
    "imported",
    "old blog",
    "write.as", "sueños",
]
---


Very boring, although the sensations were interesting, I may do some refinement on this to try to transmit them, it was all facial and body expressions.

It was way more than this but I only remember this part.

I was on a bus sitting alone but there were other people in it, destination unknown but it was some part of the country I live in. Somehow without really talking I made friends with the guys at the bus, they all new each others even tho they were on different seats, I was like the odd one there, but as I said we ended up friends.

We went through mountains and valleys, then into a city, the view was always amazing.

Once we reached out the bus central, we ended up distributed in some seats and bench around some like visitor area we talked a little about things that I don't remember, then a girl came, she was young like 13 or 14, she has this aura of smileness but also threatening, a bit yandere but also crazy-joyful/happy. She met me at first and all the others were lowkey expecting something, she went up to me and said nothing, then for some reason we also ended up kinda friends we talked about again I don't remember while I was sitting on a bench and she was standing, now for some reason she or someone in the group ended up with the backpack I was carrying, all my things for school that I had the next day and the day before were in there so it was a significant loss. If I remember correctly it was an accident, some of the dudes was helping me taking my things off by carrying them, then once out of the bus we forgot giving me my backpack back. Once we and the girl stopped chatting she did a sign with her hand and all the dudes of the buss starting moving and formed a group with her in the middle and we said goodbye and parted ways, they still kept my backpack since we still haven't realized, they went on their way all merrily and I stayed on a bench resting for a while, then a vision like came, I was like on a open field within a campus it was the school and we all were in lines there was a hardened instructor and then I suddenly remember or relized my backpack was missing he was saying some incoherent bullshit as my anxiety rose, then I had another vision but this was like a connection, the girl that was like the boss of the party realized that my backpack was with them and she ordered to give it back by the next day, then the vision close and I went back to the 'normality' of my dream. 

I stood up from the bench now more calmed after knowing I would probably get my backpack before I actually needed it, and went to grab something to eat, the food area was like in the corner but quite open and with the easy access with the outside, it was a bunch of benchs rounded up by foodstalls (not mobile, they had their own places in the building), they were about the same thing just different flavors with a different classic (speciality), it was like a burrito but not really a burrito nor a taco it was like a middle ground between them two and the tortilla was not from corn but from wheat.

The classics were, a pizza like roll but that looked more like a taco, and another had a pizza like roll but of peperoni that actually looked like a pizza, some was classic burrito like, meat, guacamole, cheese, etc. There were others consisting in different kinds of different preparations of meat, I didn't know what to pick, I was drawn to the peperoni pizza roll but, that would be just a pizza roll not the speciality of the town.

So I just went on a bench to think about it, and all of them were full except one with to dudes eating, when I approached there was a leftover, a glass with barely any of milk, but it was there, the guys sitting called out someone from another table that was on the corner and connected to the cinema, the dude came to the table, took the milk and leaved with it, I sat down and wondered about all the flavours sorrounding me and which one to pick, I asked the guys or someone else a girl, that approached me, I have memory of that too, the point is they recommended the classic burrito like one, so I went for it I think and I ordered it, shortly after that the guys stopped eating and they left leaving me alone in the table, then my burritaco came and I started eating it, I don't remember the flavor nor how I felt but a girl came and sat down with me, the same from before (?) And we started chatting in a friendly way, but we seemed with interest in each other, soon after that I realized people around us lowkey were shitting their pants, the girl was in fact girlfriend or ex of the dude that was like the boss or something that came and took his milk, I went up to him after he called for the girl and she went, she was standing next to the table in which he and his goons were sitting. He was like trying to get her back or something as he was shitting on me with no reason since we have never met, I don't remember what I told him but it was with a low aggression, like a calmed threat and then I grabbed the girl from the waist and we went to the cinema. 

Shortly after I woke up.
