---
title: I was running, as fast as I could, not out of fear, but
date: 2020-11-20T05:41:47Z
author: "Lumin"
tags: [
    "imported",
    "old blog",
    "write.as", 
]
---

I was running, as fast as I could, not out of fear, but out of will, for a decision made, it was me giving me a chance, a shot at happiness and I was feeling it flow through me I was taking it, and then I hit a dense, brick, wall, and now, I am in shambles, my determination is just a fragile broken glass under my empty dragging corpse.

I am now just resting on my bed trying to regain momentum, so that glass no longer is glass but diamond. Diamond, to break the wall of silence. For now, that my body is just carbon I am finding out ways to shine, but my torrid being just wants to close its eyes and eternally rest.
