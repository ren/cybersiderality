---
title: A dream The mist
date: 2021-08-22T20:52:11Z
author: "Lumin"
tags: [
    "imported",
    "old blog",
    "write.as", "sueños",
]
---

#dreamjournal
For some reason tonight I dreamed with a lot of people with Asian features, some others were Latinos, I was in like a job but I wasn't really working, my mates and I were cyclically going to a store that was also like a place dinner place, but not quite a cafe, and there it was a girl that always attended us, and some other coworkers of her, and some other of our friends waiting by the tables. They were my elementary and middle school friends that I never talked with since I left school behind. Rarely I thought about them as well, in the city that we were it was very generic yet not any place I could remember, block buildings, some shorts some high, but mostly short, it was a small city with various colors per locale but they were dimmed, opaque.

As days passed I saw less and less of my people in the city, at first nor I nor anyone noticed until we saw the mist, it was a silver cloud, very slowly moving towards the city, what we and how we saw it was from the edge of the town, it was like an oasis of existence in a desolate desert, of thought, the dark fog was engulfing everything, in the horizon, there was no separation between the sky and the sand, the mist was the only thing that there was for a gradient of "dreams" and color.

People were leaving or disappearing also gradually, one on one or per small groups until one day there only was the brunette girl of the store, and like one of two friends waiting and 3 that were still with me, it was all so ominous, we all knew something was happening but we still did our same routine, it was odd, it was weird because it was something also that couldn't be ignored, the girl was feeling feeble that day, next day she wasn't even there and the mist was already within the city, on our last round we went to visit her and met with our friends but they were all missing, except one that joined us before meeting there, we talked all nervous and anxious for the future, we step out of the store, the mist is all around us and only thing we can see is a pickup, we move towards it. I wake up.
