---
title: Quiero compartir mi realidad contigo, pero antes de hac
date: 2023-02-05T22:59:31Z
author: "Lumin"
tags: [
    "imported",
    "old blog",
    "write.as", 
]
---

Quiero compartir mi realidad contigo, pero antes de hacerlo quiero estar seguro de que es algo que tú también deseas. Quiero que sea una unión, no un arrebato.

The question is... do you want to share yours with mine?
