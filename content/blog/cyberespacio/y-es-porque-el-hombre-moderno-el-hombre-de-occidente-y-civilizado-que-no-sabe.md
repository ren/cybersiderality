---
title: Y es porque el hombre moderno, el hombre de occidente y
date: 2022-01-28T22:04:20Z
author: "Lumin"
tags: [
    "imported",
    "old blog",
    "write.as", 
]
---

Y es porque el hombre moderno, el hombre de occidente y civilizado que no sabe lo que ha perdido por lo que es tan apacible, ha perdido la relación con su cuerpo, con su entorno, ha perdido el vivir mismo, ha perdido el escuchar y entender los árboles, ha perdido de vista a los cazadores y a las presas, no existe ya una naturaleza para él, un entorno integrado, la orquesta de las diversas canciones, ahora todos mudos a su oído o completamente incomprensibles, solo, ruido, el hombre civilizado no es parte de este planeta y se demuestra en sus acciones, en sus monumentos no a sí mismo, no a la naturaleza, pero a lo gris e insípido de su propia mente, no vive en la jungla, no vive en el bosque, ni siquiera en una planicie, no existe conjunto con la naturaleza, sino en su artificial hogar de sólida roca falsa, su entorno ha cambiado porque este ya está sordo mudo y ciego al mundo mismo y como un extranjero, un alíen invasor, ya es otro, un ser no natural, el hombre moderno, el golem de alma perdida.

What is your interest?
What is that which lingers in your mind?
