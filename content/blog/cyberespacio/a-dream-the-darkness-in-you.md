---
title: A dream, “The darkness in you”
date: 2021-10-23T18:31:46Z
author: "Lumin"
tags: [
    "imported",
    "old blog",
    "write.as", "sueños",
]
---

#dreamjournal

Not night nor day, I can't even remember the light of the sun, I don't remember the passage of time either in that state, dreams often care not for physical conceptions. At any rate there I was or me as an entity with no clear body, a soul simply floating around the darkness and the small sparkles of stars, being the void itself, it was quiet with no sensation ever attached, and I was relieving memories of past lives, of incarnations in other worlds always with this cosmic mist surrounding my body, unseeable and unrecognizable except for the others that were like me, and then I remembered the battles I had with that other, the white one, finally we were at peace, after eons, then a spark in the back of my neck or what I thought was and felt like my neck, you see in this state I am not used to feel things, so whatever caused me to feel that, was something big, not physically but metaphysically. And as such during my trance I got visions of what caused it, it was the white one, imagine a human body, a person but completely shining with pure white, and a long dark or brown hair sometimes shaping back to human.

So then I went back to taking an identifiable form as this I got into a camp, ready to wage war, I don't know on what on whom, but it was there, preparing and awaiting command, they did not notice me, as I say only he can see me, and is not like I am made of darkness here, I just hold a dark aura as I approach, they seem like Romans, all his soldiers, but they weren't, at least not the ones you know from this world, they just looked similar, at any rate I was in the camp dressed as one, and I wanted to speak with their leader to find out what was going out, he, was in the largest of tents at the top of the small hill they set camp on, fitting for a king I thought, in a tone of sarcasm, there were guards just outside, but they didn't seem to mind me, in fact, most people around weren't behaving as such, it felt like their freedom was stripped, like if through their eyes lied the ones of another, and when I finally opened that last tent I found out why.

The white one, it was there he was their leader, with the crown of spines and all, the long dark brown hair, the factions of a European with their pale white skin too, even if he was supposedly for the Middle East according to the myths, you know who I am talking about _oh savior_, — What are you doing here? I asked, I already knew the answer of course, I saw it in his eyes, and in his attack, he was waging war, but that wasn't himself, if anything this dude was the opposite of warring, no, his eyes, something was awfully off, he didn't have that proper kindly gaze, he didn't have my own abyssal eyes either, he had the eyes of a madman, they had yellow around the eyelids, it wasn't disgusting, but it wasn't make up either, he had eye bags too, but this kind not of tiredness but of rage and torment. Something happened to him, or something got a hold on him, whatever it was, I knew, it wanted death, either mine or his, and only one was to keep existing.
