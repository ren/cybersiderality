---
title: Sin sueños, sin viento
date: 2022-10-28T04:21:27Z
author: "Lumin"
tags: [
    "imported",
    "old blog",
    "write.as", 
]
---

La marea azul sin aliento
Un último suspiro de libertad
Es la muerte final en verdad
