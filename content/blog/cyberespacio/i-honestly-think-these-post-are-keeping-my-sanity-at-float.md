---
title: I honestly think these post are keeping my sanity at fl
date: 2021-05-15T00:57:50Z
author: "Lumin"
tags: [
    "imported",
    "old blog",
    "write.as", 
]
---

I honestly think these post are keeping my sanity at float.

Anyway I haven't been writing dreams because the ones I have been having are extremely indeciphrable and I really have no words to describe them, I have noticed a couple of things however:

1. I have been meeting someone that cares deeply for me, like a sister, that I most often than not, not notice until I hear her sorrow in my defeat.

2. It has become increasingly common that I dream within my dream, like if my own mind likes to make me aware of me dreaming, I don't know what to make of this, I don't know if I am truly connected to another me in another world, or if it is just a vision, I don't know why this happens, nor how, nor if we are synchronized, I also don't know if this is the mirror me of the astral or oniric plane and our way to commute is through both ourselves dreaming into one world... all I am certain, and notice are layers of dream and consciousness.
