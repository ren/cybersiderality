---
title: A dream “Comet classes”
date: 2021-10-23T20:50:55Z
author: "Lumin"
tags: [
    "imported",
    "old blog",
    "write.as", "sueños",
]
---

#dreamjournal
This one was the last dream I had this past night, it was shortly after the last one I wrote, however I don't remember the transition, that's why I am writing them separately.
I find myself on an empty classroom, seemingly dimming at night, an instant later, crowded I am surrounded by my classmates, it is a very clean and clear room, all white and sanitized, very _official_ the professor there is an actual person I remember from the last school I was in, he was giving a class, but it wasn't about what he did in my wake life, to tell you the truth I don't even remember what was he saying, by the end of it and while he is doing it, there is a girl sitting in front of me, she, is someone of the class I am in right now, when I first met her on knew of her I felt I vibe, but now, however that never arose to anything else, in this class however we talked for a bit, it was about her vision of the world she was very passionate, and we were discussing of the fundamental realities of the structure of the world, its pillars, however this understatement didn't come as words, but as a match in the mind, by the end of our discussion which was mostly unilateral, she went up to deliver something to the professor, a homework, the thing is while I barely talked, I agreed with everything or mostly all of what she told me, and I was then just there standing in a secondary place, thinking about my own homework that I haven't done yet, it was the same as what she was saying, I didn't copy it did I? We were just agreeing to our ideas, a very similar perspective without being the same, or would we...
This troubled me for a while, and then very close to waking up, I had this images in my mind, what we were talking about on another level, the synthesis of our own interactions wasn't on the current world, but the idealization and construction of a new one, where the problems that afflict this one were nonexistent, that was what she delivered as a homework, a new world and all of us there were studying these metaphysical dynamics, the creation of entire realities and _verses_ as well as their implementation into the omniverse.
