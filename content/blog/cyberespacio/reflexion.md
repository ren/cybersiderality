---
title: Reflexión
date: 2021-04-08T08:58:43Z
author: "Lumin"
tags: [
    "imported",
    "old blog",
    "write.as", 
]
---


En los años recientes ahora que soy consciente de mi misma persona y en lo que va más allá de ella me he dado cuenta de todos los errores que he cometido en mi vida, o en otras palabras de todas mis oportunidades perdidas.

Siento que tuve una infancia y una juventud desperdiciada o nunca llevada a cabo como debería haberlo hecho. Mi ser mi consciencia estaba solo enfocada en mi ego y sus designios, ignorando por completo las ansias de mi espíritu y mi propia alma, si hubiera realizado acciones distintas en el pasado, acciones que aliviaran mi alma y espíritu mi vida sería mucho más plena ahora, mucho más completa, habría vivido mi propia vida en profundidad y vigorosidad, me sentiría el día de hoy completamente vivo y con toda la fuerza de mi ser, en vez de la enclenque versión de mi que soy ahora... mi juventud arrebatada por mi mismo y mi propia cobardía.

Aveces tienes que hacer lo más contraintuitivo, lo más insensato, al margen de la locura para aliviar los anhelos de tu alma y espíritu, estos aveces gritan sangre y violencia y hay que dárselos, es lo natural, lo justo, las reglas que preceden al hombre y sus leyes, las reglas del universo, por las que todo lo que existe aquí es regido, lo que se siente bien y sacia el espíritu y el alma. Has entonces sólo lo que te haga sentir bien, pero debes saber que esto llamara al conflicto y estar preparado para ello. 
