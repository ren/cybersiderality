---
title: You don't want to control your mind, you want to dance 
date: 2020-12-26T19:49:35Z
author: "Lumin"
tags: [
    "imported",
    "old blog",
    "write.as", 
]
---

You don't want to control your mind, you want to dance with it, to experience existence, to be and not to be.
