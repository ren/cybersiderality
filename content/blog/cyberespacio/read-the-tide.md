---
title: Read the tide
date: 2021-06-10T00:50:08Z
author: "Lumin"
tags: [
    "imported",
    "old blog",
    "write.as", 
]
---

Surf above
Dive below

It is always dark
Is it an infinite darkness?
A dragon soul screaming in the wild
I feel like I am loosing it all
Sweetest prison, sweetest cell
A wrenching roar
It hurts, all my vision now in black and grey
Only thing I feel now is pain

Will I be able to save her?
I am now blind and broken
And silence as usual is all I get
How do we change?
How do I change?
Are things stale because I am not changing?
Is the silence made by my own inaction?
All I hear is my muted screams into the void

It looks similar but it isn't the same
They are echoes of different events
Of different worlds, of different lives
And way even beyond of that
In the sea of the unseen, I see her
Slips of what I think is her
Is this just me associating or is this real
I lay defeated in my bed remembering the past days
And there is no one to lay beside me
All, really truly alone in my echo-chamber of emotions

"I know who you pretend I am"
"Why not me?"
"Why not me?"

There is no or else
There is no looming threat
There is only the now
Stripped of all clothing, of all masks
What is the thing that remains
When there is no you anymore
What is your essence?
What is your spirit?

"I" transcend, but there is no I
Concept of persona is no longer important
It no longer exists, yet something does
The spirit, the soul, which is not another ego
There is only the mind 
There are no limits
Imagination takes away my dissolved being
And from a total perspective into one I come again
But I am now another, I am not what I used to be
I changed, I ceased to be and I was again
Another ego, another persona
The ever shifting nature of the soul
Yet it being the only thing that remains constant
Its own change

Yet the question remains
Can I save her of her own mind?
I am willing to do it, I always was
I exist for no reason
I am a wildcard, always was
That is my role
That is my spirit
Freedom
Uncertainty
Fear
Chaos
Awe

There is no destiny, there is nothing set
Oh fearful star, what are we gonna do now?
I am saving her or am I just making it worse?
Should I just let go?

In reality my saving are just my own ideas
But are not others ideas already a influence
I have doubts about me and my path.. but
I trust my body
I trust my nature
I trust my guts
I trust my will
I trust my spirit
I trust my world and my ideas
They are my own
I reached them through experience
It is my mental world
It is a senseless mess
But it is not a bought ideology
And even less it is a stapled construct
It is personal
It is human
It is not an established institution
Can they say the same?

I trust myself, but I am lacking conviction
I need something else
Something is missing
My strength
Why is it missing?
How do I get it back?

Maybe another sight
At the marvels of eternity
Is what is in place
