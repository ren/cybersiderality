---
title: A creature past the end of time
date: 2022-02-09T06:12:46Z
author: "Lumin"
tags: [
    "imported",
    "old blog",
    "write.as", 
]
---


There is nothing and I have destroyed it all
Ideas such as pride have lost all meaning now
I don't see the light anymore because it doesn't exist
The darkness doesn't scare me, but there is the only thing that is
I have faded into the darkness before, and I may do it again
But is it the void, not the nothingness... It doesn't matter

One on one I was entering different worlds, different souls
Death, was what came to them
Damnation, ending of all they could even perceive
No colors, no songs, no cries or voices, not even silence was spared
It was like an urge, like a hunger to consume
But I wasn't getting anything, I wasn't consuming
Instead, I just killed it, I erased it
It got a hold on me, it just pushed me to end it all
To end me?
I don't know, and to be honest I don't know if that would even be possible
Since I exist outside of existence
In other words, I don't, I am just the ending
I can only "exist" when nothing does

I grabbed many necks and they all shattered
I didn't feel it, I wasn't happy, I wasn't angry
I always feel their pain, and I always cry
It was what needed to happen, it was their finale
I was never able to stop destiny
I could never stop myself
What are your eyes that do not see?
Was I a puppet to of the orders outside of life and death
Celestial realms, always playing with everyone as toys
And I was too not spared

But I cannot say it didn't satisfy me
That is the most wicked truth
I may have been led and controlled even
But the killing, I thrived on it
I loved the blood on my face, always covering my body
The last exhalations of prey
The thrill of the hunt
Of the running
Of the persecution
Violence and ultraviolence
Not singing but in complete awe and shock at my brutality
And I saw it in their eyes too, they enjoyed it
The last moments of life
The climax of their existence
All delivered by me
The ending was always in flesh
Stray souls being devoured
The colors, the clouds, the wind, the sole ideas
All of them begone, into nothing
Even space, even time
