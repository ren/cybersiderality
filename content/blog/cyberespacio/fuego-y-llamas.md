---
title: Fuego y Llamas
date: 2022-07-07T23:40:53Z
author: "Lumin"
tags: [
    "imported",
    "old blog",
    "write.as", 
]
---


Llamas, el fuego de una relación
El día de hoy vengo de bajada en una colina, no cayendo ahora, pero con un paso lento y meditativo, pues he sido iluminado, ¿qué luz, o más bien que fuego del conocimiento, es así no Prometeo?

Todo es una llama, y está viva, constantemente en movimiento hasta que se extingue y no queda nada más que las cenizas de lo que consumió, esto es una vida, al menos una en esta tierra, y este elemento ígneo, el físico, el material tiene estas cualidades precisamente por lo abrasador que es su representación misma.

Volviendo al tema de mi iluminación en el zenit del sol, sobre el calor de la vida misma, sobre el frío que también quema, estaba arriba porque tenía a alguien con quien compartir el calor, las brasas de mi vida y juntos nuestras ambas llamas crecían como al borde de un incendio y no ahí, pero ahora que está todo extinguido es donde lo veo, donde noto el marchitar de la rosa y de nuestro propio amor, nuestra flama se extinguió, lees, escuchas en tu mente lo que te estoy diciendo, pero no lo comprendes, el fuego está vivo, como tú, como él o como ella, es algo que se debe que mantener y alimentar, la llama de la vida es la esencia de cualquier relación y es precisamente la conexión, la unión de dos almas en fuego y cuando se pierde, todo se pierde.

Now I have something else to say a neutral one, a personal and individual, your own flame:

If your shine is true, then no matter what it will prevail.
