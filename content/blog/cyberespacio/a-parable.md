---
title: A parable?
date: 2021-07-13T08:35:39Z
author: "Lumin"
tags: [
    "imported",
    "old blog",
    "write.as", 
]
---

A parable?

The sun is at its zenith, my body is heated and dumped in sweat, such are the troubles of physical bodies, my mind is slowly losing its own composure, I need water, I know only of one place that can help me with my dilemma, but going there is pricey as its keeper always whistles a riddle, knowing this I prepare for the bath on mana.

It is a bit different than what I remember, a couple of centuries do have its toll in even already ancient ruins, she is there Maya, as ever wary of the intruders of her Oasis, but she knows me and my soul, I am not exactly a friend but we have had our talks about eternity before. She notices me in her roaming for an entrance into her white and pristine but forgotten and avoided temple, people always have a hard time facing the truth. A pillar rolls over and lets me pass, she is aware of me, I fool around a bit "Oh Oracle of the deep seas I come to you when I am most in need". A rumble is heard behind me, the entrance is sealed - come closer harlequin, her voice now sounds like a tired old woman, I wonder what happened to her.

I approach with a smile, how are you Maya? the female statue has seen much better days, she has an arm completely missing now and looks down on me with her frugal and candid orange glowy eyes, she and her usual sight of fire, as her temple her statue is white and also thoroughly deteriorated with a lot of green moss haltingly growing around her structure. The days one by one have long passed and we are now at the nightfall of our lives Dimator.

I can't get old I say - its not about the body, it is about the mind, your current state will soon cease to exist as will mine. I drink from the pool and she starts telling her story as I meld into the visions induced by the mana.

It feels like if I were falling from the cloudy sky but I feel no wind, and no weight there is only a slight sensation of vertigo and release I recover my vision and I am in a blank space, but this isn't nothingness, I hear her voice it is a riddle, it is a maze, it is anger flowing through my veins again, what was pulsating, the ardent sun was my own rage, the cool and calming mana has allowed me to see this, but why am I raging? Oblivion, the same oblivion that all beings must face, not death in my case, but being forgotten forever, even an eternal and immortal being, not even the legends may survive once I am gone from this world, me and  Maya know this, my own precence here was predestined, one last goodbye.

I open my eyes, woken up by a chilling breeze, I see the full moon on her brightest, illuminating all the pond and giving the ruins a cosmic shine, I turn to look upon the statue of my old friend, her eyes no longer shine, the arm that she has left is pointing to the skies, and her hand just below the moon as if she was holding it. I rest laying in ground and lose myself into the dark void within the stars, cold and beautiful as my own being and desires. My brief experience in this world comes to an end as my body merges with the stars. Farewell Maya.
