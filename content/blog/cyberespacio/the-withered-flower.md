---
title: The withered flower.
date: 2021-08-22T21:21:06Z
author: "Lumin"
tags: [
    "imported",
    "old blog",
    "write.as", 
]
---


Its violet purpureal petals
It's the brightest showered in moonlight
Swift traces of white merge into the color full flower
Making little sweet cakes within it

Suddenly a frond fails
The flower is sad and it is crying
But its tears are its own self
As the lovely caring blossom lives to yearn

And she cries, and she cries
Since all the colors that there are now
Violet, Purple, Green, the white creamy light of the moon
And the invisible dark void which the flower lives on

But it isn't because of its loneliness
It is simply the passage of time
The flower changes and changes
Until no longer it is one

And the bees, the bees no longer fly around.
This is the last dance.
