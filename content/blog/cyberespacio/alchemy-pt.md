---
title: Alchemy Pt. 1
date: 2022-02-09T06:24:31Z
author: "Lumin"
tags: [
    "imported",
    "old blog",
    "write.as", 
]
---


Since this is a preliminary viewpoint, I will be brief.
Alchemy as I understand it today refers to the complete mental perspective or qualia of a given moment, it is all the sensations and interpretations your conscious and unconscious experience may entice, it is what defines that instant, the current present as is, it is the undecipherable, what is seen and not, it is the complete perspective of reality at the present moment.

You can spot alchemy and alchemist by their doings, they more often than not leave scrapes, sort of notes or byproducts or direct messages in the form of coincidences, of synchronicities. If you have an avid sight you can see it in the water, in the reflection of the sun rays on it, on the unreality, surrealism or magical reality of the present time, of the moment of influx, furthermore not only at the moment itself but most noticeable in interaction between elements, don't look for the details, but feel them, feel what they show you, not see what they show you, they speak at the moment and the language of stone and fire, of mercury, thus you don't see with your eyes but with your soul, and is a mirror that looks both ways, inwards and outwards.
