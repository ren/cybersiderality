---
title: La luz del viento
date: 2022-10-28T06:34:01Z
author: "Lumin"
tags: [
    "imported",
    "old blog",
    "write.as", 
]
---


Me gusta hablar de la noche porque es lo que mejor entiendo y lo más cercano a mi mente, pues cuando cierro los ojos, no veo en blanco.

Sin embargo, hoy no escribiré sobre la noche. Pues todo el evento fue uno diurno, incluso aunque lo este relatando ahora en la madrugada.

Las hojas caen y un gran periodo de letargo se aproxima, pero no se siente real, se siente distante, ajeno, y aún así no se pierde del horizonte mismo. Y es así no por la ya letargada luz del verano o por el sol del otoño, es porque hay otros brillos en el mundo, o consciamente en lo que veo o en lo que vi hoy, no fue un amanecer, pero si un destello, quizá incluso de una esperanza perdida a la que ya era yo ciego, de una chispa que creí ya no existía, una bella rareza, una reina de la primavera enmedio del ocaso del año, ahí enmedio de todo y fuera de lugar, pero no perdida, al contrario, era lo extraordinario lo que la marcaba.

Así que no fue el sol lo que me deslumbró este día, y no fue una luz cálida ni tenue, fue una luz vívida, no flameante como la del fuego pero con el mismo vigor de esta. Era luz viva, o la luz de la vida, del mundo, del universo, la flama que pasa por tu cuerpo pero que no quema, la flama que es líquida, que pasa por las venas, no la sangre sino el viento mismo, el aire, la fuerza que se siente al correr, al hablar, al gritar, al pintar al ser y el existir... al brillar, y esa luz tuya es la más deslumbrante que hay en el mar del azar.

Y yo solo la observaba desde una posición distante incluso aunque estuviera cerca de ella, no por querer alejarme o distanciarme, por ser yo el portador de la noche, pues de hecho las estrellas solo se ven brillar en la oscuridad. No, tengo muchas capas tan abultadas que no me permiten bailar, incluso aunque fuera lo único que quisiera hacer, un claroscuro de verdad.

Todo son imágenes, ¿Qué es lo que ves?
