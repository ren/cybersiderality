---
title: Silence
date: 2021-06-11T06:05:00Z
author: "Lumin"
tags: [
    "imported",
    "old blog",
    "write.as", 
]
---

Why does she always leaves when I arrive...
What have I done?
What haven't I done?
Is it my fault?
Is it something that I am unable to see?
I feel the machinations
The constructs of nefarious ideas
Are they mine in origin?
I won't let it harm her
But I don't know how to stop it

I have seen the possibilities of our worlds
A glimpse of it to be precise
I however don't know how to transmit my vision
Maybe I shouldn't
Maybe she has to find her own way of experiencing our worlds

There is something I want to say however
Things did changed
And not for the better
I sense an internal war that has no meaning
No purpose, no reason to be
Is not even my war but I am in it
And everyone is hurting each other
Alleged reasons and broken hearts
And as I was in the war she got caught in it
A casualty or a warrior
Only she can decide her own fate
I can help her
I can guide her with all my mistakes
But she has to snap out of it herself
And she needs to break the silence
I can lend her my eyes to see beyond
But you can only save your own

I am here, not there
I do not save
I protect
I guide and rise
If I were there 
A life we would share
My body, all yours
Your body, all mine
Into one we would melt
And from the ashes
We both shall arise
Please get up
And stand with me
