---
title: Virtuality / Virtualidad (?)
date: 2020-10-28T20:42:46Z
author: "Lumin"
tags: [
    "imported",
    "old blog",
    "write.as", 
]
---

Virtuality / Virtualidad (?)

En una reflexión sobre la situación actual, el encierro voluntario domiciliario, me hizo observar y recordar un poco la época en la que yo era un adolescente, allá a los quince, en el 2012, hace ya bastantes años desde mi perspectiva actual, tenía esta idea de que todo lo que importaba para mi, todo lo que valoraba de alguna forma debía que ser real, eran los inicios de la era digital, o más bien los inicios de su accesibilidad y popularización, existía este chat, live (?) de windows, creo se llamaba así, es lo que se usaba principalmente, cuando apenas estaba creciendo facebook. 

Utilizaré un ejemplo para clarificación, en esos días de lo que más ansiaba o deseaba era una pareja sin siquiera saber porque o lo que aquello significaba, y sin embargo era cobarde aún en muchas maneras, estaba dañado, y eso ataba mi lengua en un gran nudo, me congelaba con las chicas muy a menudo y mi única forma de comunicarme era virtualmente, chats, y este tipo de plataformas, mi error, mis heridas eran y son internas y la solución también lo era y es. De cualquier manera me sentía más cómodo al hablar virtualmente sin embargo tenía esa espina de que nada sería real mientras fuera virtual, y en cierta manera esta idea contiene algo de veracidad, pero no esta arraigada en ello, sino en tradición y miedo o cautela a lo desconocido, a lo nunca antes visto por nuestros ancestros, la era digital; tecnología en la punta de nuestros dedos, es ciertamente un nuevo periodo de la humanidad y creo que lentamente nos damos cuenta de ello, mi generación... nací en 1997, tengo memorias de la época digital de los 90s y 2000s, pero crecí y viví en la de los 2010s y he experimentado este cambio de paradigma, soy un pequeño puente de comprensión en ese sentido, entre el entendimiento de lo digital como algo extraordinario a la cotidianidad de la experiencia humana, a casi la parte central de esta, y todo solo en el margen de 10 años.

Lo que realmente quiero decir, es que la virtualidad se ha normalizado a tal punto que para personas como yo, personas probablemente en mi margen de edad que estábamos atrapados entre el viejo y el nuevo paradigma, esta pandemia ha roto nuestros lazos con las tradiciones de comportamiento inconsciente del pasado, o al menos es como el periódico gotear del agua sobre una roca, lenta pero constantemente erosionándola. Y gran parte de esto se lo debemos a la generación que nos sigue, los zoomers, la generación Z que nació en medio del cambio de paradigma, para ellos no hay un cambio ni paradigma viejo, pues la normalidad radica en la hyperconectividad, y no existe estigma ya para relaciones sociales en virtualidad.

Y otra de las razones de esto es la posmodernidad y perdida de valores ¿Qué importancia tiene el crear una relación física o en persona? ¿Cuál es la razón más allá de la tradición? Preguntas que navegan el mar del inconsciente colectivo de mi generación, la que sigue y la anterior, la respuesta como es usual, nada, un valor neutro sin pros ni contras, y sin embargo, el peso de la soledad, del aislamiento, de la desconexión, son razones, son motivos para olvidar o sustituir estos comportamientos culturales impuestos y vivir más allá de la realidad, de la atadura material del cuerpo, del tiempo y el espacio, de la distancia, superando todos estos limitantes y obstáculos mediante la virtualidad.

In other words, people just got tired of being alone due to old beliefs that are not even their own, each one has a life and decisions to make, and we are promted or pushed to choose in this event of isolation.
