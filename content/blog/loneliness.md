---
title: "Loneliness"
date: 2023-02-25T18:37:20-06:00
author: "Lumin"
draft: true
tags: [
    "reflexión",
    "español",
    "soledad",
]
---

Tu aquí leyendo esto, pensando sobre el silogismo y la verdad de la existencia y la experiencia, creo que las personas son reales porque no podría soportar el peso completo de la soledad, me aplastaría por completo hasta ser más pequeño que un átomo, y después, nada.

No, Tiene que ser un texto entero sobre la soledad.

Sobre el no tener absolutamente a nadie con quien expirementar y vivir la vida.
