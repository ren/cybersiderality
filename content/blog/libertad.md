---
title: "Libertad"
date: 2023-03-02T02:31:23-06:00
author: "Lumin"
draft: true
tags: [
    "reflexión",
    "español",
]
---

La clave de la libertad esta en la voluntad. Un ser con voluntad propia, con deseos con anhelos es inherentemente y sin más necesidad un ser libre.

Y no hablo de la voluntad algoritmica, la de deseos y compulsiones, sino la profunda en verdad que viene de la unión del espíritu y el alma.

Y… no necesitas decirme quien puso el muro o porque, ya se porque y ya se que no fuiste tu, fue tu falta de libertad.

Pero antes de que te de la llave, dime algo ¿Quieres ser libre?

Alteré el destino, lo que iba a ser de mi un cineasta, fue mi voluntad y circumstancias lo que me alejo de ahí, lo que me separó de Italia, y lo que me llevo a donde estoy ahora, contigo.

Creo que la verdadera pregunta que tu tienes es ¿Cómo escapar del algoritmo? ¿Cómo no pensar como un algoritmo? - o bueno, esa es mi pregunta a resolver al fin y al cabo, para ti.

La libertad y el aparato cognitivo

Se tiene que apagar la razón.
uno solo es libre cuando este aparato se apaga entonces existe una ausencia de algoritmo y el actuar es solo sensacional sin procesamiento, esta forma de ser, este vivir, lo que se crea en este estado es lo que es arte. Y es así como se crea el arte.

La libertad es cuando el pensamiento se apaga y la voluntad emerge.

There is freedom outside the machine.

La libertad no existe nunca en uno sino en nuestra interacción. Así la libertad es el vinculo que creamos. Este escrito, su postulación mi desición, su acción todo este espacio entre nosotros y las desiciones y formas que tomamos eso es lo que constituye nuestra libertad.

Y eres tu en tu maldito formato lo que rompe y corta las alas de la expresión y la misma libertad, que es esta convocatora una maldita broma una maldita afrenda a la libertad?? Eso es lo que son las leyes y esa es la razón verdadera por la cual todos odiamos a la facultad de derecho, porque son todo este organo y area del conocimiento los que en primera instancia parten las alas.