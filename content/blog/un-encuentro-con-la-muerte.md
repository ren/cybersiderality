---
title: "Un encuentro con la muerte"
date: 2024-02-01T20:08:50-06:00
author: "Lumin"
tags: [
    "cuentos",
    "español",
	"narrativa",
]
---

Esta historia esta inspirada en un sueño que tuve hace un par de semanas que me despertó en medio de la noche, con miedo, porque se sentía como si hubiera sido un mensaje algo real… en ese sueño me había hecho mejor amigo de la muerte.

Al inicio no lo eramos, era de hecho y sin saberlo mi contrincante, estaba intentando evitar la muerte, pero no la mía, la de un otro o una otra quizás, y nos enfrascamos en batallas y batallas sobre el tiempo y através de mundos y universos, pero siempre era el mismo tejido, la misma historia, la misma narrativa que nos ataba, era una pelea contra la muerte misma, contra su caza, robandole a su presa.

Al final, todos sabemos que la muerte es algo inevitable, quizá tanto para este mundo como para muchos otros, las cosas se disuelven, se esfuman, desaparecen, el existir tiene una expiración, son verdades que nos hemos repetido durante mucho tiempo, lo vi, lo vivi, lo sentí, sentí lo inevitable, pero no me detuve e incluso en ese punto donde nos habíamos ambos salido del todo, en el punto en el que no hay ni cielo ni espacio, en las sensaciones más allá del vacío donde nos encontramos, ahí lo entendí, en la trascendencia, era una historia que nosotros mismos habíamos hecho, de intentos fallidos y de lo inevitable, pero no estaba enojado con ella ni ella conmigo, cuando nos encontramos ahí encontré en ella una cálida sonrisa, ella me entendía, y yo la entendía a ella.

No era una advertencia, no era una caza a mi o a quien amaba, no era una batalla sin fin, y tampoco era algo del todo inevitable, era podría decirse, una lucha de voluntades, mi voluntad por salvar a alguien y la voluntad de ella por extinguir lo que debe de cesar de existir, ambos estabamos ahora en el entendimiento de nuestra lucha, era algo digno, incluso honorable, ella me sonrío y yo desperté, sabía que había encontrado a la muerte y esta me había sonreido a los ojos.