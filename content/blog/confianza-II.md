---
title: "Confianza II y El hombre sin ojos"
date: 2023-03-07T01:26:07-06:00
author: "Lumin"
tags: [
    "reflexión",
    "español",
]
---

Pienso que de hecho la realidad esta constituida por una red de confianza inter e intra individual.

Folk, volken, pueblo. Del pueblo y para el pueblo. El espíritu unido permanece, solo por estos pequeños hilos entretejidos que constituyen la realidad.

Hilos que solo unos cuantos actores clave conocen, ven, sienten y viven.

¿Es el destino o la realidad misma hablando?

¿Qué es este relato? ¿Quién soy yo? ¿Quién eres tú?

Sin respuestas como siempre o quizás con todas pero no sabiendo como expresarlas. Es difícil cuando no existen en ningún lenguaje hablado.

¿Cúal es la prosa entonces?

Una canción, una sensación. Y tu sabes que la escuchas, porque yo la escucho también y así es como hablamos los dos.

Este es uno de los hilos invisibles, lo estás leyendo y descifrando ahora querida.

Y yo ni me di cuenta cuando lo escribía.

Así es, nada más que los garabatos de un ciego.

Y aún así... ¿porqué siempre retumba el corazón?

Estoy aquí.

Y tu estas aquí, para todas mis sorpresas.

Es ahora el gran momento de hacer un mundo nuevo.

Un evento cósmico más allá de la realidad del mundo.

Ya no puedo tener ningún momento de miedo...

Lentamente el momento verdadero se acerca en la imposibilidad del mundo... porque yo también lo vengo sintiendo todo.

Nos leeremos.

Nos veremos.

Sonreiremos.

Hasta entonces...

Mi corazón...

No, no siento mi corazón.

Y cada vez estoy más perdido en las tinieblas.

¿Serás tú?

Me gustaría poder abrir los ojos.
