---
title: "Lo Romantico Del Misterio"
date: 2023-03-31T23:02:18-06:00
author: "Lumin"
draft: true
tags: [
    "reflexión",
    "español",
]
---

_En algún punto tienes que parar a cuestionarte y preguntarte si el hambre y voracidad que tenemos por saber es algo que verdaderamente debamos seguir, si no es lo intangible algo que deba de mantenerse de esa manera, sumergido en un misterio completo._

Lo estaba pensando así porque una vez que se forma conocimiento desde una perspectiva epistemológica este se enraiza y enzarza a una única forma tautológica de percibir la realidad, es decir, una vez a algo se le asigna un significado o una razón este permanece y resuena consigo mismo y así es como se crea un filtro perceptivo de esa verdad. La razón no es algo que ortogue así certeza o una verdad, sino simplemente una respuesta congruente con un marco epistemológico previo, así es como funcionan los axiomas, las matemáticas y el conocimiento construido.

Al intentar llamar lo innombrable, lo inimaginable y lo imaginable, lo inalcanzable la contemplación misma el suceso el misterio y la magia se pierden, lo que se siente, la efimerencia del momento, la burbujeante ola y las sensaciones del presente, es eso de lo que se puede hablar y se habla siempre pero nunca se descifra, es esto en efecto prueba de la existencia de lo romántico.